package cz.vse.hotelsemestral.service;

import cz.vse.hotelsemestral.entity.Reservation;
import cz.vse.hotelsemestral.entity.Room;
import cz.vse.hotelsemestral.repository.ReservationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReservationService implements IReservationService {

    @Autowired
    ReservationRepo reservationRepo;

    @Override
    public List<Reservation> getAllReservations() {
        return reservationRepo.findAll();
    }

    @Override
    public List<Reservation> getAllReservationsByClient(int id) {
        return reservationRepo.findAllByClient(id);
    }

    @Override
    public void updateReservation(Reservation reservation) {
        reservationRepo.saveAndFlush(reservation);
    }

    @Override
    public void createReservation(Reservation reservation) {
        reservationRepo.saveAndFlush(reservation);
    }

    @Override
    public Reservation getReservationById(int id) {
        return reservationRepo.findById(id);
    }


}
