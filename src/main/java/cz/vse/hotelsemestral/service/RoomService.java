package cz.vse.hotelsemestral.service;

import cz.vse.hotelsemestral.entity.Room;
import cz.vse.hotelsemestral.logic.Methods;
import cz.vse.hotelsemestral.repository.RoomRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class RoomService implements IRoomService {

    @Autowired
    private RoomRepo roomRepo;

    @Override
    public void createRoom(Room room) {
        roomRepo.saveAndFlush(room);
    }

    @Override
    public Room  getRoomByName(String name) {
        return roomRepo.getRoomByName(name);
    }

    @Override
    public List<Room> getAvailableRooms(Date checkIn, Date checkOut) {
        return Methods.removeDuplicates((ArrayList<Room>) roomRepo.getAvailableRooms(checkIn, checkOut));
    }


}
