package cz.vse.hotelsemestral.repository;

import cz.vse.hotelsemestral.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ReservationRepo extends JpaRepository<Reservation, Integer> {

   @Query(value = "SELECT * FROM reservation JOIN client on reservation.client_id = client.id where client.id = :id", nativeQuery = true)
   List <Reservation> findAllByClient ( @Param("id")int id);

   Reservation findById(int id);
}
