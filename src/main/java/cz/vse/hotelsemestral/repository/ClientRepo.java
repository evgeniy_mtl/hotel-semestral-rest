package cz.vse.hotelsemestral.repository;

import cz.vse.hotelsemestral.entity.Client;
import cz.vse.hotelsemestral.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ClientRepo extends JpaRepository <Client, Integer> {

    @Query("SELECT client FROM Client client WHERE client.email = :email")
    Client getClientByEmail(@Param("email")String email);

    Client findById(int id);
}
