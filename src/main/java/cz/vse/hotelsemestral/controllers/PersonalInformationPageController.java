package cz.vse.hotelsemestral.controllers;

import cz.vse.hotelsemestral.entity.Client;
import cz.vse.hotelsemestral.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
public class PersonalInformationPageController {

    @Autowired
    ClientService clientService;

    @GetMapping("/personal-information")
    public String personalInformation(@RequestParam(name="roomName", required=true) String roomName,
                                      @RequestParam(name="checkIn", required=true) String checkIn,
                                      @RequestParam(name="checkOut", required=true) String checkOut,
                                      Model model, Principal principal){

        if (principal != null){
            model.addAttribute("principal","true");
        } else {
            model.addAttribute("principal","false");
        }

        model.addAttribute("roomName", roomName);
        model.addAttribute("checkIn", checkIn);
        model.addAttribute("checkOut", checkOut);
        return "personalInformation";
    }

}
